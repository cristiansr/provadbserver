Fiz um sistema para organizar e verificar "qual restaurante as equipes da DBServer ir�o"
Basicamente o menu inicial faz com que voc� escolha a equipe, e depois verifique o pr�ximo restaurante clicando no bot�o.
Al�m disso tem a visualiza��o da lista da equipe, prefer�ncia, e se foi visitado.
Pode editar os funcion�rios, as equipes e os restaurantes.

O banco de dados � FAKE.

- O que vale destacar no c�digo implementado?
	- Vale destacar que os models est�o prontos para receberem as a��es do banco de dados.
	- Existe tamb�m uma pequena valida��o s� para verificar se a informa��o est� vazia.
	- O min�no de registro por funcion�rio, equipe e restaurante � 1.
	- Se caso equipe n�o tiver nenhum funcion�rio ou nenhum restaurante, a tela indicar� "Erro" com a possibildade de voltar para o Home ou acessar qualquer aba.

- O que poderia ser feito para melhorar o sistema?
	- Ter um dropdown, onde voc� poderia ver s� por equipe ou restaurante, tanto na lista de prefer�ncia, tanto na p�gina de edi��o dos funcion�rios.
	- Trocar a maneira de execu��o da tela inicial de verifica��o para ajax.
	- O feedback de erro na verifica��o, faltava detalhar o erro.