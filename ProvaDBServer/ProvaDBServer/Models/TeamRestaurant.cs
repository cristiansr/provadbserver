﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Web;

namespace ProvaDBServer.Models
{
    public class TeamRestaurant
    {
        public static List<TeamRestaurant> teamsRestaurants = new List<TeamRestaurant>();
        public static int idTeamCheckChoosed = 0;
        private static bool _initialized = false;

        public int TeamRestaurantId { get; set; }
        public int TeamId { get; set; }
        public int RestaurantId { get; set; }
        public bool IsVisited { get; set; }
        public int Preference { get; private set; }

        public TeamRestaurant(int teamRestaurantId ,int teamId, int restaurantId, bool isVisited)
        {
            this.TeamRestaurantId = teamRestaurantId;
            this.TeamId = teamId;
            this.RestaurantId = restaurantId;
            this.IsVisited = isVisited;
            this.Preference = 0;
        }

        public static void Init(bool force = false)
        {
            if (_initialized)
            {
                if (!force)
                    return;
            }

            //Clear Preferences
            teamsRestaurants.ForEach(tr => tr.Preference = 0);

            List<Employee> employees = Employee.employees;

            foreach (Employee emp in employees)
            {
                if (emp.RestaurantId > -1 && emp.TeamId > -1)
                {
                    TeamRestaurant getTr = teamsRestaurants.Find(tr => tr.TeamId == emp.TeamId && tr.RestaurantId == emp.RestaurantId);
                    if (getTr == null)
                    {
                        getTr = new TeamRestaurant(teamsRestaurants.Count == 0 ? 0 : teamsRestaurants[teamsRestaurants.Count - 1].TeamRestaurantId + 1, 
                            emp.TeamId, emp.RestaurantId, false);
                        teamsRestaurants.Add(getTr);
                    }

                    getTr.Preference++;
                }
                else
                {
                    // Remove Restaurant Is Equal Null
                    TeamRestaurant getTr = teamsRestaurants.Find(tr => tr.TeamId == emp.TeamId && tr.RestaurantId == emp.RestaurantId);
                    teamsRestaurants.Remove(getTr);
                }
            }

            for (int i = 0; i != teamsRestaurants.Count; i++)
            {
                if (teamsRestaurants[i].Preference == 0)
                {
                    teamsRestaurants.RemoveAt(i);
                    --i;
                }
            }

            Sort();

            _initialized = true;
        }

        public static void Sort()
        {
            //Sort Restaurants
            List<Restaurant> restaurants = Restaurant.restaurants;
            teamsRestaurants.Sort((tr1, tr2) => restaurants.Find(restaurant => restaurant.RestaurantId == tr1.RestaurantId).Name.CompareTo(
                restaurants.Find(restaurant => restaurant.RestaurantId == tr2.RestaurantId).Name));

            //Sort Preference
            teamsRestaurants.Sort((tr1, tr2) => tr2.Preference.CompareTo(tr1.Preference));

            //Sort Teams
            List<Team> teams = Team.teams;
            teamsRestaurants.Sort((tr1, tr2) => teams.Find(team => team.TeamId == tr1.TeamId).Name.CompareTo(
                teams.Find(team => team.TeamId == tr2.TeamId).Name));
        }

        public static void Sort(List<TeamRestaurant> trs)
        {
            if (trs.Count < 2) //Dont remove if have less than 2
                return;
            
            //Sort Restaurants
            List<Restaurant> restaurants = Restaurant.restaurants;
            trs.Sort((tr1, tr2) => restaurants.Find(restaurant => restaurant.RestaurantId == tr1.RestaurantId).Name.CompareTo(
                restaurants.Find(restaurant => restaurant.RestaurantId == tr2.RestaurantId).Name));

            //Sort Preference
            trs.Sort((tr1, tr2) => tr2.Preference.CompareTo(tr1.Preference));

            //Sort Teams
            List<Team> teams = Team.teams;
            trs.Sort((tr1, tr2) => teams.Find(team => team.TeamId == tr1.TeamId).Name.CompareTo(
                teams.Find(team => team.TeamId == tr2.TeamId).Name));
        }

        public static TeamRestaurant GetNextRestaurant (int teamId)
        {
            List<TeamRestaurant> trs = teamsRestaurants.FindAll(tr => tr.TeamId == teamId);

            int idChoose = 0;
            bool allVisited = true;

            if (trs.Count > 1)
            {
                Sort(trs);
                for (int i = 0; i != trs.Count; i++)
                {
                    if (!trs[i].IsVisited && trs[i].Preference > 0)
                    {
                        idChoose = i;
                        allVisited = false;
                        break;
                    }
                }

                if (allVisited)
                    trs.ForEach(tr => tr.IsVisited = false);
            }

            TeamRestaurant getTr = trs.Count > 0 ? trs[idChoose] : null;

            return getTr;
        }

        public string PercentPreference()
        {
            int numberOfEmployees = Team.GetNumberOfEmployees(this.TeamId);

            float percent = 100 * (float)this.Preference / (float)numberOfEmployees;

            return percent.ToString("##0") + "%";
        }
    }
}