﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProvaDBServer.Models
{
    public class Restaurant
    {
        public static List<Restaurant> restaurants = new List<Restaurant>()
        {
            new Restaurant(0, "Le Grand Burger"),
            new Restaurant(1, "Restaurante do seu Telmo"),
            new Restaurant(2, "Atelier de Massas"),
            new Restaurant(3, "Restaurante Anita")
        };

        public int RestaurantId { get; set; }
        [DisplayName("Nome")]
        [Required(ErrorMessage = "Preencha o nome")]
        public string Name { get; set; }

        public Restaurant()
        {
            this.RestaurantId = -1;
            this.Name = "";
        }

        public Restaurant(int restaurantId, string name)
        {
            this.RestaurantId = restaurantId;
            this.Name = name;
        }

        public void Insert ()
        {
            restaurants.Sort((r1, r2) => r1.RestaurantId.CompareTo(r2.RestaurantId));
            restaurants.Add(new Restaurant(restaurants[restaurants.Count - 1].RestaurantId + 1, this.Name));
        }

        public void Update()
        {
            Restaurant getRestaurant = restaurants.Find(t => (t.RestaurantId == this.RestaurantId));
            getRestaurant.Name = this.Name;
        }

        public void Delete()
        {
            restaurants.Remove(this);
        }
    }
}