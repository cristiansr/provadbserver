﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace ProvaDBServer.Models
{
    public class Employee
    {
        public static List<Employee> employees = new List<Employee>()
        {
            new Employee(0, "Cristian Ritter", 0, 0),
            new Employee(1, "Estéfani Silva", 0, 0),
            new Employee(2, "Rudinei da Silva", 0, 1),
            new Employee(3, "Fernando Souza", 0, 2),
            new Employee(4, "João Paulo", 0, 3),
            new Employee(5, "Marcelo Dias", 0, 3),
            new Employee(6, "Alberto Perreira", 0, 0),
            new Employee(7, "Marcos Aguiar", 1, 2),
            new Employee(8, "Tiago Mello", 1, 0),
            new Employee(9, "Júlia Lima", 1, 1),
            new Employee(10, "Roberto Augusto", 1, 1),
            new Employee(11, "Romário Cruz", 1, 2),
            new Employee(12, "Adalberto Flores", 1, 3),
            new Employee(13, "Juan Rodrigues", 2, 0),
            new Employee(14, "Cláudio Alonso", 2, 2),
            new Employee(15, "Ivan Martinello", 2, 3),
            new Employee(16, "Lucas Coelho", 2, 3),
            new Employee(17, "Vitor Küng", 2, 1),
            new Employee(18, "Nathan Moura", 2, 3),
            new Employee(19, "Carlos Alberto", 3, 1),
            new Employee(20, "Rebeca Antunes", 3, 2),
            new Employee(21, "Antônio Moureira", 3, 2),
            new Employee(22, "João Pedro", 3, 3),
            new Employee(23, "Tiago Klos", 3, 0),
            new Employee(24, "Ana Paula", 3, 2),
            new Employee(25, "Rafael da Costa", 3, 2)
        };

        public int EmployeeId { get; set; }
        [DisplayName("Nome")]
        [Required(ErrorMessage = "Preencha o nome")]
        [StringLength(50, ErrorMessage = "Nome tem limite de 50 caracteres")]
        public string Name { get; set; }
        [DisplayName("Equipe")]
        public int TeamId { get; set; }
        [DisplayName("Restaurante")]
        public int RestaurantId { get; set; }

        public Employee()
        {
            this.EmployeeId = -1;
            this.Name = "";
            this.TeamId = 0;
            this.RestaurantId = 0;
        }

        public Employee(int id, string name, int teamId, int restaurantId)
        {
            this.EmployeeId = id;
            this.Name = name;
            this.TeamId = teamId;
            this.RestaurantId = restaurantId;
        }

        public static void Sort()
        {
            //Sort Employees
            employees.Sort((e1, e2) => string.Compare(e1.Name, e2.Name));

            //Sort Teams
            List<Team> teams = Team.teams;
            employees.Sort((e1, e2) =>
            {
               return string.Compare(teams.Find(team => team.TeamId == e1.TeamId).Name, 
                    teams.Find(team => team.TeamId == e2.TeamId).Name);
            });
        }

        public void Insert()
        {
            employees.Sort((r1, r2) => r1.EmployeeId.CompareTo(r2.EmployeeId));
            employees.Add(new Employee(employees[employees.Count - 1].EmployeeId + 1, this.Name, this.TeamId, this.RestaurantId));
        }

        public void Update()
        {
            Employee getEmployee = employees.Find(t => (t.EmployeeId == this.EmployeeId));
            getEmployee.Name = this.Name;
            getEmployee.TeamId = this.TeamId;
            getEmployee.RestaurantId = this.RestaurantId;
        }

        public void Delete()
        {
            employees.Remove(this);
        }

        public void UpdateTeam(int id)
        {
            this.TeamId = id;
        }

        public void UpdateRestaurant(int id)
        {
            this.RestaurantId = id;
        }
    }
}