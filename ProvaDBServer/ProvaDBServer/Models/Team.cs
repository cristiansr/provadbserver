﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProvaDBServer.Models
{
    public class Team
    {
        public static List<Team> teams = new List<Team>()
        {
            new Team(0, "501"),
            new Team(1, "701"),
            new Team(2, "703"),
            new Team(3, "705")
        };

        public int TeamId { get; set; }
        [DisplayName("Nome")]
        [Required(ErrorMessage = "Preencha o nome")]
        public string Name { get; set; }

        public Team()
        {
            this.TeamId = -1;
            this.Name = "";
        }

        public Team(int teamId, string name)
        {
            this.TeamId = teamId;
            this.Name = name;
        }

        public static int GetNumberOfEmployees(int teamId)
        {
            List<Employee> employees = Employee.employees;

            int count = 0;
            foreach (Employee emp in employees)
            {
                if (emp.TeamId == teamId)
                    count++;
            }

            return count;
        }

        public void Insert()
        {
            teams.Sort((r1, r2) => r1.TeamId.CompareTo(r2.TeamId));
            teams.Add(new Team(teams[teams.Count - 1].TeamId + 1, this.Name));
        }

        public void Update()
        {
            Team getTeam = teams.Find(t => (t.TeamId == this.TeamId));
            getTeam.Name = this.Name;
        }

        public void Delete()
        {
            teams.Remove(this);
        }
    }
}