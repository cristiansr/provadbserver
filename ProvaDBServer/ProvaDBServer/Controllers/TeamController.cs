﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProvaDBServer.Models;

namespace ProvaDBServer.Controllers
{
    public class TeamController : Controller
    {
        // GET: Team
        public ActionResult Index()
        {
            Team.teams.Sort((t1, t2) => t1.Name.CompareTo(t2.Name));

            return View(Team.teams);
        }

        public ActionResult Create()
        {
            Team team = new Team();
            return View(team);
        }

        [HttpPost]
        public ActionResult Create(Team team)
        {
            if (ModelState.IsValid)
            {
                team.Insert();

                return ReturnToIndex();
            }

            return View(team);
        }

        public ActionResult Edit(int id)
        {
            List<Team> teams = Team.teams;
            Team team = teams.Find(t => (t.TeamId == id));

            return View(team);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Team team)
        {
            if (ModelState.IsValid)
            {
                team.Update();

                return ReturnToIndex();
            }

            return View(team);
        }

        public ActionResult Delete(int id)
        {
            List<Team> teams = Team.teams;
            Team team = teams.Find(t => (t.TeamId == id));

            return View(team);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int teamId)
        {
            List<Team> teams = Team.teams;
            teams.Find(t => (t.TeamId == teamId)).Delete();
            List<Employee> employees = Employee.employees.FindAll(employee => employee.TeamId == teamId);
            employees.ForEach(employee => employee.UpdateTeam(-1));

            //return teams[id].name;
            return ReturnToIndex();
        }

        ActionResult ReturnToIndex()
        {
            //TeamRestaurant.Init(true);

            return RedirectToAction("Index");
        }

        public static List<SelectListItem> GetList(Employee employee = null)
        {
            List<SelectListItem> teamItems = new List<SelectListItem>();
            if (employee == null)
            {
                bool first = true;
                foreach (Team team in Team.teams)
                {
                    teamItems.Add(new SelectListItem
                    {
                        Text = team.Name,
                        Value = team.TeamId.ToString(),
                        Selected = first
                    });

                    first = false;
                }
            }
            else
            {
                foreach (Team team in Team.teams)
                {
                    teamItems.Add(new SelectListItem
                    {
                        Text = team.Name,
                        Value = team.TeamId.ToString(),
                        Selected = team.TeamId == employee.TeamId ? true : false
                    });
                }
            }

            return teamItems;
        }

    }
}