﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProvaDBServer.Models;

namespace ProvaDBServer.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            Employee.Sort();

            return View(Employee.employees);
        }

        public ActionResult Create()
        {
            List<SelectListItem> teamItems = TeamController.GetList();
            List<SelectListItem> restaurantItems = RestaurantController.GetList();

            ViewBag.TeamId = teamItems;
            ViewBag.RestaurantId = restaurantItems;

            Employee employee = new Employee();

            return View(employee);
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.Insert();

                return ReturnToIndex();
            }

            List<SelectListItem> teamItems = TeamController.GetList(employee);
            List<SelectListItem> restaurantItems = RestaurantController.GetList(employee);

            ViewBag.TeamId = teamItems;
            ViewBag.RestaurantId = restaurantItems;

            return View(employee);
        }

        public ActionResult Edit(int id)
        {
            List<Employee> employees = Employee.employees;
            Employee employee = employees.Find(t => (t.EmployeeId == id));

            List<SelectListItem> teamItems = TeamController.GetList(employee);
            List<SelectListItem> restaurantItems = RestaurantController.GetList(employee);

            ViewBag.TeamId = teamItems;
            ViewBag.RestaurantId = restaurantItems;

            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.Update();

                return ReturnToIndex();
            }

            List<SelectListItem> teamItems = TeamController.GetList();
            List<SelectListItem> restaurantItems = RestaurantController.GetList();

            ViewBag.TeamId = teamItems;
            ViewBag.RestaurantId = restaurantItems;

            return View(employee);
        }

        public ActionResult Delete(int id)
        {
            List<Employee> employees = Employee.employees;
            Employee employee = employees.Find(t => (t.EmployeeId == id));

            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Employee employee)
        {
            employee.Delete();

            return ReturnToIndex();
        }

        ActionResult ReturnToIndex()
        {
            TeamRestaurant.Init(true);

            return RedirectToAction("Index");
        }
    }
}