﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProvaDBServer.Models;

namespace ProvaDBServer.Controllers
{
    public class RestaurantController : Controller
    {
        // GET: Restaurant
        public ActionResult Index()
        {
            Restaurant.restaurants.Sort((r1, r2) => r1.Name.CompareTo(r2.Name));

            return View(Restaurant.restaurants);
        }

        public ActionResult Create()
        {
            Restaurant restaurant = new Restaurant();
            return View(restaurant);
        }

        [HttpPost]
        public ActionResult Create(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                restaurant.Insert();

                return ReturnToIndex();
            }

            return View(restaurant);
        }

        public ActionResult Edit(int id)
        {
            List<Restaurant> restaurants = Restaurant.restaurants;
            Restaurant restaurant = restaurants.Find(t => (t.RestaurantId == id));

            return View(restaurant);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                restaurant.Update();

                return ReturnToIndex();
            }

            return View(restaurant);
        }

        public ActionResult Delete(int id)
        {
            List<Restaurant> restaurants = Restaurant.restaurants;
            Restaurant restaurant = restaurants.Find(t => (t.RestaurantId == id));

            return View(restaurant);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int restaurantId)
        {
            List<Restaurant> restaurants = Restaurant.restaurants;
            restaurants.Find(t => (t.RestaurantId == restaurantId)).Delete();
            List<Employee> employees = Employee.employees.FindAll(employee => employee.RestaurantId == restaurantId);
            employees.ForEach(employee => employee.UpdateRestaurant(-1));

            return ReturnToIndex();
        }

        ActionResult ReturnToIndex()
        {
            //TeamRestaurant.Init(true);

            return RedirectToAction("Index");
        }

        public static List<SelectListItem> GetList(Employee employee = null)
        {
            List<SelectListItem> restaurantItems = new List<SelectListItem>();
            if (employee == null)
            {
                bool first = true;
                foreach (Restaurant restaurant in Restaurant.restaurants)
                {
                    restaurantItems.Add(new SelectListItem
                    {
                        Text = restaurant.Name,
                        Value = restaurant.RestaurantId.ToString(),
                        Selected = first
                    });

                    first = false;
                }
            }
            else
            {
                foreach (Restaurant restaurant in Restaurant.restaurants)
                {
                    restaurantItems.Add(new SelectListItem
                    {
                        Text = restaurant.Name,
                        Value = restaurant.RestaurantId.ToString(),
                        Selected = restaurant.RestaurantId == employee.RestaurantId ? true : false
                    });
                }
            }

            return restaurantItems;
        }
    }
}