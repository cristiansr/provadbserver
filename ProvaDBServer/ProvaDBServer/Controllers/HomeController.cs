﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProvaDBServer.Models;

namespace ProvaDBServer.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            TeamRestaurant.Init(true);

            List<SelectListItem> teamItems = new List<SelectListItem>();
            int i = 0;
            foreach (Team team in Team.teams)
            {
                teamItems.Add(new SelectListItem
                {
                    Text = team.Name,
                    Value = team.TeamId.ToString(),
                    Selected = i == TeamRestaurant.idTeamCheckChoosed ? true : false
                });

                i++;
            }

            ViewBag.TeamId = teamItems;

            return View(Team.teams);
        }

        public ActionResult List()
        {
            TeamRestaurant.Init(true);

            return View(TeamRestaurant.teamsRestaurants);
        }

        public ActionResult Search(int teamId)
        {
            TeamRestaurant teamRestaurant = TeamRestaurant.GetNextRestaurant(teamId);
            TeamRestaurant.idTeamCheckChoosed = Team.teams.FindIndex(team => team.TeamId == teamId);

            if (teamRestaurant == null)
            {
                return RedirectToAction("Error");
            }

            return View(teamRestaurant);
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RestaurantSearchConfirmed(int teamId)
        {
            TeamRestaurant teamRestaurant = TeamRestaurant.GetNextRestaurant(teamId);
            teamRestaurant.IsVisited = true;

            return RedirectToAction("Index");
        }
    }
}